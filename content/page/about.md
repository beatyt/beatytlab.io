---
title: About me
subtitle: What I can do for you
comments: true
---

### Skills and what I can offer

#### Application integrations.

I've integrated with many different APIs:

- Stripe for handling payments within a Typescript app
- Discord for a websockets based chatbot
- Steam WebAPI for pulling Dota 2 match data
- Geomaps for data population based on zip codes
- and more!

#### A polyglot developer.

Java, Go (this site uses Hugo), JavaScript, Gatsby, Elm, SQL, NoSQL, React, AWS, Firebase...

By exploring new languages, we discover new ideas and principles. As such, I continue to dedicate time each week to keeping up with trends in my trade.
 
For example, Learning Express taught me that there was a much quicker way to build a REST service than an entire Java .war deployed to a container. That skill has been very applicable to improving my own Java integrations, and improving on tooling.

#### Excellent debugging skills.

I've fixed _many_ performance issues:

- Slow pages due to queries? ✅ 
- Multi-threading for IO operations? ✅
- Fixing errors that occurred on another thread? ✅
- Adding retry logic to requests? ✅

### My history

I started coding in JASS (just another scripting syntax) for the WC3 modding engine. Someone on AIM taught me what a boolean expression was.
