---
title: A Pattern for Clean and Reusable BluePrints in UE5
date: 2023-06-26
tag: ["game", "ue5", "blueprints"]
---
_How to create blueprints once, and avoid duplicating your logic across multiple actors._

# The Problem
In our game, Act on Instinct, we wanted to have three types of actors:
1. Characters
2. Vechicles*
3. Buildings

All of these actors can receive damage from different sources. But, we don't want to have to maintain duplicating logic and having three systems for damaging these actors. How can we accomplish this?

_* the vehicles have armor plates that take damage based on direction._

# Solution
_Interfaces and actor components to the rescue!_

**Interfaces** in software development are signatures without any logic. They let me tell you what something does, without you having to know how it works. In UE5, we have **Blueprint Interfaces** that provide this similar functionality.

⚠️ In general, I recommend putting as many things behind interfaces as you can, even your UI widgets. When you want to create a new implementation, you can swap out the new implementation as long as it implements the interface. Then you avoid having to update N blueprints that rely on that old blueprint.

**Actor components** in UE5 allow us to create a single instance attached to an actor.

Now, onto the step-by-step.

## Concept

Here is the problem, visualized.

![Blueprint](/images/clean-bp/excalidraw-bp.png)

Every actor will implement the interface, and then use the actor component to reuse the logic.

# Step-by-step

1. Create actor component `BP_DamageableComponent`
2. Add components to `Character`, `Building`, etc.

![Blueprint](/images/clean-bp/otTCt0yeFI.png)
_Attaching the component to the `Actor`_

3. Create the `BP_DirectionalDamageableComponent`
4. Attach to the `Vehicle`

![Blueprint](/images/clean-bp/ue.png)
_Integration_

# Another problem, we only want partially different functionality!

In our Vehicle, we want the directional damage, but want to keep the same logic for healing. How can we accomplish this?

In my solution, I use an abstract class/blueprint as the parent.

![Blueprint](/images/clean-bp/excalidraw-dmgeable-component.png)

⚠️ Be judicious in using abstract blueprints. You can end up with confusing hierarchies that other people will not understand. I try to never go more than 1 level in depth. And usually it should be self-contained that no one else will have to maintain.

Then, we have the below. This provides us the functionality of taking damage by direction.

![Blueprint](/images/clean-bp/ue-2.png)

With this, we have reusable logic for a damaging system.
