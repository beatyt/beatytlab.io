---
title: A working typescript integration test for Firebase
date: 2019-11-01
tag: ["solutions"]
---

There is a lack of documentation on typescript with firebase functions.

Below is a working integration test, adapted from the nodejs documentation

Note: The process part will depend on your OS.

```
import assert = require('assert');
import chai from 'chai';
import chaiHttp from 'chai-http';
import 'mocha';

chai.use(chaiHttp);
// let childProcessPromise: ChildProcessPromise<PromiseResult<string>>;


describe('profile', function() {
    before(() => {
        // need to kill process
        // what OS will build server run?
        // childProcessPromise = exec(`firebase emulators:start`);
    });

    after(async () => {
        // await childProcessPromise;
    });

    it('should get a profile', function () {
        return chai.request('http://localhost:5001')
            .get('/vul-cws/us-central1/doGetUserProfile?phoneNumber=4445555555')
            .then((res: any) => {
                chai.expect(res).to.have.status(200);
            }).catch(e => {
                console.log(e);
                assert.fail();
            });
    });
});
```
