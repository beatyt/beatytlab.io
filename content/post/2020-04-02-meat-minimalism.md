---
title: Meat minimalism
date: 2020-04-02
tag: ["personal"]
---

Meat minimalism, a term I first heard coined by my father, is the practice of cutting out a large percentage of animal products without making one's life miserable. It's a practice that I adopted a few years ago and has been sustainable for me where vegetarianism and veganism weren't. I'd like to make my case for the practice and list the benefits.

## Substitute as much as possible

I avoid the animal products I don't enjoy in a bizzare Marie Kondo dietary program way. In practice, this means that I don't do ground beef. I've found that tacos are _better_ if you just use black beans instead. For burgers, I usually only tasted the toppings anyway, so a plant based burger works just as well.

My substitutions:

Instead of ground beef, use black beans.
Instead of a beef or chicken patty, use a plant-based one.
Instead of dairy milk, use almond milk.

## When do I cheat?

When I will truly enjoy an animal product and appreciate every moment of the meal, that's when I'll eat meat. A pit-roasted rack of ribs is absolutely on the table, but it happens very occasionally.

Also when I travel... ordering at a restaurant in a foreign country that doesn't speak English natively gets tricky. So, I allow myself to just enjoy a vacation and try some new things.

## The Benefits

1. My grocery bill has gone down by a lot! I'm only paying around $100/mo. on groceries.

2. Never worry about food spoiling again. When I accidentally leave the food out overnight, there's no meat spoiling and ruining everything. Still not a good idea to leave stuff out.

3. I feel good that I'm helping sustainability and the environment.

4. I can continue to travel. I like to travel and try new foods. When you're in a country where English isn't the native language, ordering food can be tricky. Under this practice, I can enjoy my vacations without being problematic to other people.
