---
title: Bugs in Hackerrank during an interview
date: 2019-10-21
tag: ["solutions"]
---

Had this come up during an assessment from hackerrank...

After running their cucumber tests in the browser, the console output was garbled and incomprehensible.

My next thought, was to run them locally. They flat out failed on my machine.

This line is used for calling the cucumber tests:

```
<argument>java -jar ${project.build.directory}/${project.build.finalName}.jar</argument>
```

When it reaches this line in `runner.js`:

`this.child = spawn(command, {`

it fails due to failing to normalize paths.

To test this, I just changed my jar argument to the hardcoded directory and was able to receive test results.

Also,

One must drill into `node_modules/@c1-code-test/take-home-tester/dist/steps` to actually obtain the steps being run for the tests. They're not included with the checkout.

...

I reported this bug to hackerrank and haven't heard back from them at all.
